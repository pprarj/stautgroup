<?php
    class loginController {
        public function __construct() {
            header('Access-Control-Allow-Origin: *');
			header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
			header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
			$method = $_SERVER['REQUEST_METHOD'];
			if($method == "OPTIONS") {
				die();
            }
        }

        public function index() {
            header('Content-type: application/json; charset=utf-8');

            $result = array();
            $data = json_decode(file_get_contents('php://input'));
            $user = new users();

            echo json_encode($user->doLogin($data->email, $data->password));
        }
    }
?>