<?php
	class model {
		protected $db;

		public function __construct() {
			$settings['dbname'] = 'stautgroup';
			$settings['host'] = 'localhost';
			$settings['dbuser'] = 'root';
			$settings['dbpass'] = '';

			$this->db = new PDO("mysql:dbname=".$settings['dbname'].";host=".$settings['host'], $settings['dbuser'], $settings['dbpass']);
		}
	}
?>