<?php
	class users extends model {
        public function __construct() {
            parent::__construct();
        }

        public function create_user($user) {
            $findUser = $this->getUser($user['email']);

            if (isset($findUser['email'])) {
                return "Usuário já existente!";
            } else {
                $sql = $this->db->prepare("INSERT INTO users SET name = :name, email = :email, password = :password, drink_counter = 0");
                $sql->bindValue(":name", $user['name']);
                $sql->bindValue(":password", md5($user['password']));
                $sql->bindValue(":email", $user['email']);

                if ($sql->execute()) {
                    return 'Usuário criado!';
                } else {
                    return 'Não foi possível criar este usuário neste momento. Tente novamente mais tarde!';
                }
            }
        }

        public function deleteUser($id) {
            $sql = $this->db->prepare("DELETE FROM users WHERE id = :id");
            $sql->bindValue("id", $id);

            if ($sql->execute()) {
                return 'Usuário deletado!';
            } else {
                return 'Houve um erro ao tentar excluir este usuário! Tente novamente mais tarde!';
            }
        }

        public function doLogin($email, $pass) {
            $user = $this->getUser($email);

            if ($user) {
                if (md5($pass) == $user['password']) {
                    $sql = $this->db->prepare("SELECT * FROM users WHERE email = :email");
                    $sql->bindValue(":email", $email);
                    $sql->execute();
        
                    $array = array();
                    if ($sql->rowCount() > 0) {
                        $row = $sql->fetch(PDO::FETCH_ASSOC);
                        
                        if (md5($pass) == $row['password']) {
                            $array['token'] = md5(date('Y-m-d H:i:s'));

                            if(!$this->insertToken($email, $array['token'])) {
                                return $array['error'] = "Ocorreu um erro inexperado. Tente novamente ou fale com o administrador";
                            }

                            $array['iduser'] = $row['id'];
                            $array['email'] = $row['email'];
                            $array['name'] = $row['name'];
                            $array['drink_counter'] = $row['drink_counter'];
                        } else {
                            $array['error'] = "Senha inválida!";
                        }
                    } else {
                        $array['error'] = "Login inválido!";
                    }
        
                    return $array;
                } else {
                    return 'Senha inválida!';
                }
            } else {
                return 'Usuário inexistente!';
            }
        }

        public function drink($id, $ml) {
            $drink_counter = $this->getUserById($id);
            
            $sql = $this->db->prepare("UPDATE users SET drink_counter = :drink_counter WHERE id = :id");
            $sql->bindValue("id", $id);
            $sql->bindValue("drink_counter", $drink_counter['drink_counter'] + $ml);

            if ($sql->execute()) {
                return "Contador atualizado! Você bebeu " . ($drink_counter['drink_counter'] + $ml) . " ml.";
            }
        }

        public function editUser($id, $userInfo) {
            $sql = $this->db->prepare("UPDATE users SET email = :email, name = :name, password = :password WHERE id = :id");
            $sql->bindValue("id", $id);
            $sql->bindValue("email", $userInfo['email']);
            $sql->bindValue("name", $userInfo['name']);
            $sql->bindValue("password", md5($userInfo['password']));

            if ($sql->execute()) {
                return 'Informações atualizadas!';
            } else {
                return 'Erro! Não foi possível atualizar as informações. Tente novamente mais tarde!';
            }
        }

        public function getUser($email) {
            $sql = $this->db->prepare("SELECT * FROM users WHERE email = :email");
            $sql->bindValue(":email", $email);
            $sql->execute();

            $row = array();
            if ($sql->rowCount() > 0) {
                $row = $sql->fetch(PDO::FETCH_ASSOC);
            }

            return $row;
        }

        public function getUserById($id) {
            $sql = $this->db->prepare("SELECT * FROM users WHERE id = :id");
            $sql->bindValue(":id", $id);
            $sql->execute();

            $row = array();
            if ($sql->rowCount() > 0) {
                $row = $sql->fetch(PDO::FETCH_ASSOC);
            }

            return $row;
        }

        public function getUsers() {
            $sql = $this->db->prepare("SELECT email FROM users");
            $sql->execute();

            $row = array();
            if ($sql->rowCount() > 0) {
                $row = $sql->fetchAll(PDO::FETCH_ASSOC);
            }

            return $row;
        }

        public function insertToken($email, $token) {
            $sql = $this->db->prepare("UPDATE users SET token = :token WHERE email = :email");
            $sql->bindValue("token", $token);
            $sql->bindValue("email", $email);

            if ($sql->execute()) {
                return true;
            } else {
                return false;
            }
        }

        public function verifyToken($id, $token) {
            $sql = $this->db->prepare("SELECT token FROM users WHERE id = :id");
            $sql->bindValue("id", $id);
            $sql->execute();

            if ($sql->rowCount() > 0) {
                $row = $sql->fetch(PDO::FETCH_ASSOC);

                if ($row['token'] == $token) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
?>