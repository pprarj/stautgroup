<?php
    class usersController {
        public function __construct() {
            header('Access-Control-Allow-Origin: *');
			header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
			header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
			$method = $_SERVER['REQUEST_METHOD'];
			if($method == "OPTIONS") {
				die();
            }
        }

        public function index($idUser = '', $drink_counter = '') {
            header('Content-type: application/json; charset=utf-8');

            $users = new users();

            switch ($_SERVER['REQUEST_METHOD']) {
                case 'GET':
                    if (!isset($_SERVER['HTTP_TOKEN'])) {
                        echo json_encode('Informe o token no header da requisição');
                        break;
                    }

                    if ($idUser == '') {
                        echo json_encode($users->getUsers());
                    } else {
                        $user = $users->getUserById($idUser);
                        $u['iduser'] = $user['id'];
                        $u['name'] = $user['name'];
                        $u['email'] = $user['email'];
                        $u['drink_counter'] = $user['drink_counter'];

                        echo json_encode($u);
                    }

                    break;
                case 'POST':
                    if ($idUser == '') {
                        $data = json_decode(file_get_contents('php://input'));
    
                        $new['email'] = addslashes($data->email);
                        $new['name'] = addslashes($data->name);
                        $new['password'] = addslashes($data->password);
    
                        echo json_encode($users->create_user($new));

                        break;
                    }

                    if (!isset($_SERVER['HTTP_TOKEN'])) {
                        echo json_encode('Informe o token no header da requisição');
                        break;
                    }

                    $data = json_decode(file_get_contents('php://input'));

                    if ($data->drink_ml == '') {
                        echo json_encode('Informe a quantidade bebida!');
                        break;
                    }

                    echo json_encode($users->drink($idUser, $data->drink_ml));

                    break;
                case 'PUT':
                    if (!isset($_SERVER['HTTP_TOKEN'])) {
                        echo json_encode('Informe o token no header da requisição');
                        break;
                    }

                    if ($idUser == '') {
                        echo json_encode('Informe o usuário!');
                        break;
                    }

                    $data = json_decode(file_get_contents('php://input'));

                    $userInfo['email'] = addslashes($data->email);
                    $userInfo['name'] = addslashes($data->name);
                    $userInfo['password'] = addslashes($data->password);

                    echo json_encode($users->editUser($idUser, $userInfo));

                    break;
                case 'DELETE':
                    if (!isset($_SERVER['HTTP_TOKEN'])) {
                        echo json_encode('Informe o token no header da requisição');
                        break;
                    }

                    if ($idUser == '') {
                        echo json_encode('Informe o usuário!');
                        break;
                    }

                    if (!$users->verifyToken($idUser, $_SERVER['HTTP_TOKEN'])) {
                        echo json_encode('O token informado está inválido! Faça login novamente!');
                        break;
                    }

                    if ($users->getUserById($idUser)) {
                        echo json_encode($users->deleteUser($idUser));
                    } else {
                        echo json_encode('Usuário não encontrado!');
                    }


                    break;
            }
        }
    }
?>