<?php
	class core {
		public function run() {
            $url = $_SERVER['REQUEST_URI'];

            if (!empty($url)) {
                $url = explode('/', $url);
                array_shift($url);
                array_shift($url);

                $currentController = $url[0].'Controller';
                array_shift($url);
                $currentAction = 'index';
                
                if (count($url) > 0) {
					$params = $url;
				} else {
                    $params = array();
                }
            }

            $c = new $currentController();
			call_user_func_array(array($c, $currentAction), $params);
        }
    }
?>
